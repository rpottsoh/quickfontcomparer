unit uDetailDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  PK.FontList.Default, FMX.ScrollBox, FMX.Memo, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, PK.LimitSize;

type
  TfrmDetailDialog = class(TForm)
    layoutBottom: TLayout;
    btnVersionClose: TButton;
    memoDetail: TMemo;
    SizeGrip1: TSizeGrip;
    Layout1: TLayout;
    lblFontName: TLabel;
    procedure memoDetailApplyStyleLookup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnVersionCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private var
    FLimitSize: TLimitSize;
    FBackColor: TAlphaColor;
  public
    class procedure New(
      const iOwner: TCommonCustomForm;
      const iText: String;
      const iFontProperty: TFontProperty;
      const iTextSettings: TTextSettings;
      const iBackColor: TAlphaColor);
  end;

implementation

{$R *.fmx}

{ TfrmDetailDialog }

procedure TfrmDetailDialog.btnVersionCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmDetailDialog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

procedure TfrmDetailDialog.FormCreate(Sender: TObject);
begin
  FLimitSize := TLimitSIze.Create(Self);
  FLimitSize.Parent := Self;
  FLimitSize.SetLimit(320, 240, 0, 0);
end;

procedure TfrmDetailDialog.memoDetailApplyStyleLookup(Sender: TObject);
var
  R: TRectangle;
begin
  if memoDetail.FindStyleResource<TRectangle>('background', R) then
    R.Fill.Color := FBackColor;
end;

class procedure TfrmDetailDialog.New(
  const iOwner: TCommonCustomForm;
  const iText: String;
  const iFontProperty: TFontProperty;
  const iTextSettings: TTextSettings;
  const iBackColor: TAlphaColor);
begin
  with TfrmDetailDialog.Create(iOwner) do
  begin
    FBackColor := iBackColor;

    StyleBook := iOwner.StyleBook;
    ApplyStyleLookup;

    if iFontProperty = nil then
      lblFontName.Text := ''
    else
      lblFontName.Text := iFontProperty.ToString;

    memoDetail.TextSettings.Assign(iTextSettings);
    memoDetail.Text := iText;
    memoDetail.SelStart := iText.Length;
    memoDetail.WordWrap := True;
    memoDetail.SetFocus;

    Show;
  end;
end;

end.
