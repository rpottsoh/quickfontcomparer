unit uVersionDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts;

type
  TfrmVersion = class(TForm)
    textTitle: TText;
    textVersion: TText;
    textCopyrightProgram: TText;
    btnVersionClose: TButton;
    textPlatform: TText;
    listTitleUnder: TLine;
    layoutCopyrightBase: TLayout;
    textCopyrighDesign: TText;
    layoutBottom: TLayout;
    imgIcon: TImage;
    imgBuiltWith: TImage;
    procedure imgBuiltWithClick(Sender: TObject);
  private
  public
    class procedure New(
      const iForm: TCommonCustomForm;
      const Style: TStyleBook);
  end;

implementation

{$R *.fmx}

uses
  PK.Utils.Browser;

procedure TfrmVersion.imgBuiltWithClick(Sender: TObject);
begin
  OpenBrowser('https://www.embarcadero.com/jp/products/delphi');
end;

class procedure TfrmVersion.New(
  const iForm: TCommonCustomForm;
  const Style: TStyleBook);
begin
  with TfrmVersion.Create(iForm) do
    try
      StyleBook := Style;
      ApplyStyleLookup;
      imgIcon.SendToBack;

      btnVersionClose.SetFocus;
      ShowModal;
    finally
      DisposeOf;
    end;
end;

end.
