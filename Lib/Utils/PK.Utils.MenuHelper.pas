(*
 * Menu Helper
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2014/08/06 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.MenuHelper;

interface

uses
  System.Classes
  , FMX.Menus
  , FMX.Types
  ;

type
  TMenuHelper = class helper for TMainMenu
  public
    // トップレベルの Sub menu の Enabled を設定
    procedure SetEnabled(const Value: Boolean);
    // TLang を使ってメニューの表記言語を変更する
    procedure ChangeLang(const iLang: TLang);
  end;

implementation

uses
  System.SysUtils
  ;

{ TMenuHelper }

procedure TMenuHelper.ChangeLang(const iLang: TLang);
var
    i: Integer;

  procedure TranslateMenuText(const iMenuItem: TMenuItem);
  var
    Str: String;
    i: Integer;
  begin
    Str := Translate(iMenuItem.Text);
    if (Str.Length > 0) then
      iMenuItem.Text := Str;

    for i := 0 to iMenuItem.ItemsCount - 1 do
      TranslateMenuText(iMenuItem.Items[i]);
  end;

begin
  for i := 0 to ItemsCount - 1 do
    TranslateMenuText(TMenuItem(Items[i]));

  RecreateOSMenu;
end;

procedure TMenuHelper.SetEnabled(const Value: Boolean);
var
  i: Integer;
begin
  for i := 0 to ItemsCount - 1 do
    TMenuItem(Items[i]).Enabled := Value;
end;

end.
