(*
 * Rectangle as Dialog
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/04/08 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

 unit PK.Utils.RectDialog;

interface

uses
  FMX.StdCtrls
  , FMX.Objects
  ;

type
  TRectDialog = class
  private var
    FBase: TShape;
    FDisabler: TShape;
    FCloseButton: TCustomButton;
  private
    procedure DisablerClick(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
  protected
    property Base: TShape read FBase;
  public
    constructor Create(
      const iBase, iDisabler: TShape;
      const iCloseButton: TCustomButton = nil); reintroduce;
    procedure Show;
    procedure Hide;
  end;

implementation

uses
  FMX.Types;

{ TRectDialog }

procedure TRectDialog.CloseButtonClick(Sender: TObject);
begin
  Hide;
end;

constructor TRectDialog.Create(
  const iBase, iDisabler: TShape;
  const iCloseButton: TCustomButton = nil);
begin
  inherited Create;

  FBase := iBase;
  FDisabler := iDisabler;
  FCloseButton := iCloseButton;

  if FCloseButton <> nil then
    FCloseButton.OnClick := CloseButtonClick;
end;

procedure TRectDialog.DisablerClick(Sender: TObject);
begin
  Hide;
end;

procedure TRectDialog.Hide;
begin
  FBase.Visible := False;
  if FDisabler <> nil then
    FDisabler.Visible := False;
end;

procedure TRectDialog.Show;
begin
  FBase.Visible := True;
  if FDisabler <> nil then
  begin
    FDisabler.OnClick := DisablerClick;
    FDisabler.Align := TAlignLayout.Contents;
    FDisabler.Visible := True;
  end;
end;

end.
