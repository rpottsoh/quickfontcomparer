(*
 * Supports macOS style behavior
 *
 * PLATFORMS
 *   macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses
 *     PK.Utils.MacDummyMain;
 *
 *   type
 *     TForm1 = class(TForm)
 *       procedure FormCreate(Sender: TObject);
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     ReplaceMainFormForMac;
 *   end;
 *
 * 2018/03/28 Version 1.0.0
 * 2018/03/29 Version 1.0.1
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.MacDummyMain;

interface

procedure ReplaceMainFormForMac;

implementation

uses
  System.Classes
  , System.UITypes
  , System.SysUtils
  , System.Rtti
  , FMX.Types
  , FMX.Forms
  , FMX.Menus
  ;

type
  TOpenForm = class(TCommonCustomForm)
  end;

  TDummyMain = class(TCommonCustomForm)
  protected
    procedure MainFormClose(Sender: TObject; var Action: TCloseAction);
    function CanShow: Boolean; override;
  public
    constructor CreateNew(Owner: TComponent; Dummy: NativeInt = 0); override;
  end;

{ TDummyMain }

function TDummyMain.CanShow: Boolean;
begin
  Result := False;
end;

constructor TDummyMain.CreateNew(Owner: TComponent; Dummy: NativeInt = 0);
begin
  inherited;

  TThread.ForceQueue(
    TThread.Current,
    procedure
    var
      MainForm: TOpenForm;
      Obj: TObject;
      MM: TMainMenu absolute Obj;
      RttiType: TRttiType;
      RttiField: TRttiField;
    begin
      // MainForm 取得
      MainForm := TOpenForm(Application.MainForm);

      // OnClose を変更して閉じないようにする
      MainForm.OnClose := MainFormClose;

      // 自分を MainForm に指定
      Application.MainForm := Self;

      // Menu を載せ替える
      Obj := MainForm.MainMenu;
      if (Obj <> nil) and (Obj is TMainMenu) then
      begin
        AddObject(MM);

        RttiType := SharedContext.GetType(ClassType);
        if RttiType <> nil then
        begin
          RttiField := RttiType.GetField('FMainMenu');
          if RttiField <> nil then
            RttiField.SetValue(Self, MM);
        end;

        TMainMenu(MM).RecreateOsMenu;
      end;
    end
  );
end;

procedure TDummyMain.MainFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caHide;
end;

procedure ReplaceMainFormForMac;
begin
  {$IFDEF OSX}
  TDummyMain.CreateNew(Application);
  {$ELSE}
  // 他の OS では何もしない
  {$ENDIF}
end;

end.
