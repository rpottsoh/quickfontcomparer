﻿#LimitSize

##Overview

The LimitSize library sets the minimum and maximum window sizes.

##Environment

| Item        | Description        |
|-------------|--------------------|
| Environment | Delphi, RAD Studio |
| Version     | XE7 or later       |
| Framework   | FireMonkey         |
| Support OS  | Windows, macOS     |

##Files
| Files                   | Description                  |
|-------------------------|------------------------------|
| LICENSE.txt             | License                      |
| PK.LimitSize.pas        | LimitSize Source             |
| PK.LimitSize.Win.pas    | LimitSize Source for Windows |
| PK.LimitSize.Mac.pas    | LimitSize Source for macOS   |

##Usage

1.
Add the folder of the above file to the search path.  
Add PK.LimitSize to the uses block.  

```delphi
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,  
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,  
  PK.LimitSize; // <- Add
```

2.
Call TLimitSize.Create to create a LimitSize instance and call SetLimit.  

```delphi
type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    FLimitSize: TLimitSize;
  public
  end;

implementation

procedure TForm1.FormCreate(Sender: TObject);
begin
  FLimitSize := TLimitSize.Create(Self);
  FLimitSize.SetLimit(
    180,  // Minimum Width
    160,  // Minimum Height
    1024, // Maximum Width
    768); // Maximum Height
end;
```

##Known bug
If form uses "Premium Style" and TSizeGrip, the size is not controlled.  
Maybe, the bug is TSizeGrip's bug.

##Contact
freeonterminate@gmail.com  
http://twitter.com/pik  

#LICENSE
Copyright (c) 2017 HOSOKAWA Jun  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
