﻿(*
 * Window size limiter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2017 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.LimitSize;
 *
 *   type
 *     TForm1 = class(TForm)
 *       procedure FormCreate(Sender: TObject);
 *     private
 *       FLimitSize: TLimitSize;
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     FLimitSize := TLimitSize.Create(Self);
 *     FLimitSize.SetLimit(180, 160, 1024, 768);
 *   end;
 *
 * 2017/11/17 Version 1.0.2
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

 unit PK.LimitSize.Mac;

{$IFNDEF MACOS}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}

interface

implementation

uses
  PK.LimitSize
  , Macapi.CocoaTypes, Macapi.AppKit
  , FMX.Types, FMX.Platform, FMX.Platform.Mac
  ;

type
  TLimitSizeMac = class(TInterfacedObject, ILimitSize)
  private
    FWnd: NSWindow;
    FOrgMinSize: NSSize;
    FOrgMaxSize: NSSize;
  public
    function SetLimit(
      const AHandle: TWindowHandle;
      const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
    destructor Destroy; override;
  end;

  TLimitSizeFactoryMac = class(TLimitSizeFactory)
  private class var
    SSelf: TLimitSizeFactoryMac;
  public
    function CreateLimitSize: ILimitSize; override;
  end;

procedure RegisterLimitSizeMac;
begin
  TLimitSizeFactoryMac.SSelf := TLimitSizeFactoryMac.Create;
  TPlatformServices.Current.AddPlatformService(
    ILimitSizeFactory,
    TLimitSizeFactoryMac.SSelf as ILimitSizeFactory);
end;

{ TLimitSizeMac }

destructor TLimitSizeMac.Destroy;
begin
  FWnd.setContentMinSize(FOrgMinSize);
  FWnd.setContentMaxSize(FOrgMaxSize);

  inherited;
end;

function TLimitSizeMac.SetLimit(
  const AHandle: TWindowHandle;
  const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
var
  Size: NSSize;
begin
  FWnd := WindowHandleToPlatform(AHandle).Wnd;

  // 最小値の設定
  Size.width := FWnd.contentMinSize.width;
  Size.height := FWnd.contentMinSize.height;
  FOrgMinSize := Size;

  if (AMinWidth > 0) then
    Size.width := AMinWidth;

  if (AMinHeight > 0) then
    Size.height := AMinHeight;

  FWnd.setContentMinSize(Size);

  // 最大値の設定
  Size.width := FWnd.contentMaxSize.width;
  Size.height := FWnd.contentMaxSize.height;
  FOrgMaxSize := Size;

  if (AMaxWidth > 0) then
    Size.width := AMaxWidth;

  if (AMaxHeight > 0) then
    Size.height := AMaxHeight;

  FWnd.setContentMaxSize(Size);

  Result := True;
end;

{ TLimitSizeFactoryMac }

function TLimitSizeFactoryMac.CreateLimitSize: ILimitSize;
begin
  Result := TLimitSizeMac.Create;
end;

initialization
  RegisterLimitSizeMac;

end.
