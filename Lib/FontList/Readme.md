﻿# FontList

## Overview

The FontList library gets Font list

## Environment

| Item        | Description        |
|-------------|--------------------|
| Environment | Delphi, RAD Studio |
| Version     | Tokyo or later     |
| Framework   | FireMonkey         |
| Support OS  | Windows, macOS     |

##Usage

1.
Add the folder of the above file to the search path.
Add PK.FontList & PK.FontList.Helper to the uses block.

```pascal
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  PK.FontList, PK.FontList.Helper; // <- Add
```

2.
Call LoadFonts to get FontList to ListBox.

```pascal
type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

implementation

procedure TForm1.FormCreate(Sender: TObject);
begin
  LoadFonts(
    ListBox1,

    // Optional (specify nil when omitted)
    function(const iFontProperty: TFontProperty): Boolean
    begin
      Result := True; // Add always
    end,

    // Optional (specify nil when omitted)
    procedure
    begin
      ShwoMessage('Finish!');
    end
  );
end;
```

## Contact
freeonterminate@gmail.com
http://twitter.com/pik

# LICENSE
Copyright (c) 2018 HOSOKAWA Jun
Released under the MIT license
http://opensource.org/licenses/mit-license.php

