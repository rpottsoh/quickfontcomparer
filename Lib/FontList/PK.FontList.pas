(*
 * Font List Getter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.FontList, PK.FontList.Helper;
 *
 *   type
 *     TForm1 = class(TForm)
 *       ListBox1: TListBox;
 *       procedure FormCreate(Sender: TObject);
 *     private
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     LoadFonts(ListBox1, nil, nil);
 *   end;
 *
 * 2018/04/08 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.FontList;

interface

uses
  System.Classes
  , System.Generics.Collections
  , FMX.Types
  , PK.FontList.Default
  ;

type
  TFontList = class
  private var
    FFontList: TFontPropertyList;
  private
    function GetItems(const iIndex: Integer): TFontProperty;
    function GetCount: Integer;
  protected
    constructor Create; reintroduce;
  public
    function GetEnumerator: TFontPropertyListEnumerator;
    property Count: Integer read GetCount;
    property Items[const iIndex: Integer]: TFontProperty read GetItems; default;
  private class var
    FCurrent: TFontList;
  public
    class property Current: TFontList read FCurrent;
  end;

implementation

uses
  FMX.Platform
  {$IFDEF MSWINDOWS}
  , PK.FontList.Win
  {$ENDIF}
  {$IFDEF OSX}
  , PK.FontList.Mac
  {$ENDIF}
  ;

{ TFontList }

constructor TFontList.Create;
var
  Factory: IFontListFactory;
  FontList: IFontList;
begin
  inherited Create;

  FFontList := TFontPropertyList.Create;

  if
    TPlatformServices.Current.SupportsPlatformService(
      IFontListFactory,
      IInterface(Factory)
    )
  then
  begin
    FontList := Factory.CreateFontList;
    if FontList <> nil then
      FontList.GetFontList(FFontList);
  end;
end;

function TFontList.GetCount: Integer;
begin
  Result := FFontList.Count;
end;

function TFontList.GetEnumerator: TFontPropertyListEnumerator;
begin
  Result := TFontPropertyListEnumerator.Create(FFontList);
end;

function TFontList.GetItems(const iIndex: Integer): TFontProperty;
begin
  if (iIndex > -1) and (iIndex < FFontList.Count) then
    Result := FFontList[iIndex]
  else
    Result := nil;
end;

initialization
  TFontList.FCurrent := TFontList.Create;

finalization
  TFontList.FCurrent.DisposeOf;

end.
