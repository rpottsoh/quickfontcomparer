(*
 * Font List Getter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.FontList, PK.FontList.Helper;
 *
 *   type
 *     TForm1 = class(TForm)
 *       ListBox1: TListBox;
 *       procedure FormCreate(Sender: TObject);
 *     private
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     LoadFonts(ListBox1, nil, nil);
 *   end;
 *
 * 2018/04/08 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)


unit PK.FontList.Win;

// (*
{$IFNDEF MSWINDOWS}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}
// *)

interface

procedure RegisterFontList;

implementation

uses
  System.Classes
  , System.SysUtils
  , FMX.Platform
  , Winapi.Windows
  , PK.FontList.Default
  ;

type
  TWinFontList = class(TInterfacedObject, IFontList)
  public
    procedure GetFontList(const iList: TFontPropertyList);
  end;

  TWinFontListFactory = class(TFontListFactory)
  public
    function CreateFontList: IFontList; override;
  end;

procedure RegisterFontList;
var
  Factory: TWinFontListFactory;
begin
  Factory := TWinFontListFactory.Create;
  TPlatformServices.Current.AddPlatformService(IFontListFactory, Factory);
end;

{ TWinFontListFactory }

function TWinFontListFactory.CreateFontList: IFontList;
begin
  Result := TWinFontList.Create;
end;

{ TWinFontList }

procedure TWinFontList.GetFontList(const iList: TFontPropertyList);
var
  DC: HDC;
  LFont: TLogFont;

  function EnumFontsProc(
    var LogFont: TLogFont;
    var TextMetric: TTextMetric;
    FontType: Integer;
    Data: Pointer): Integer; stdcall;
  var
    S: TFontPropertyList absolute Data;
    LogFontEx: TEnumLogFontEx absolute LogFont;
    LogFontName, FullName: String;
  begin
    Result := 1;

    // Direct2D だと代替フォントで描画されてしまうためラスタフォントは排除
    if FontType = RASTER_FONTTYPE then
      Exit;

    LogFontName := LogFont.lfFaceName;
    FullName := LogFontEx.elfFullName;

    if not LogFontName.StartsWith('@') and not LogFontName.StartsWith('$') then
      S.Add(
        TFontProperty.Create(
          LogFontName,
          FullName,
          LogFont.lfPitchAndFamily and FIXED_PITCH <> 0,
          String(LogFontEx.elfScript) = '日本語'
        )
      );
  end;

begin
  iList.Clear;

  DC := GetDC(0);
  try
    FillChar(LFont, SizeOf(LFont), 0);
    LFont.lfCharset := DEFAULT_CHARSET;
    EnumFontFamiliesEx(DC, LFont, @EnumFontsProc, LPARAM(iList), 0);
  finally
    ReleaseDC(0, DC);
  end;

  iList.Sort;
end;

initialization
  RegisterFontList;

end.
